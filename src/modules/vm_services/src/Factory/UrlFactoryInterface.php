<?php

namespace Drupal\vm_services\Factory;

use Drupal\Core\Url;

/**
 * Interface UrlFactoryInterface.
 */
interface UrlFactoryInterface {

  /**
   * Wrapper for Url generation from user input.
   *
   * @param string $url
   *   The url object.
   * @param array $options
   *   Options array.
   *
   * @return \Drupal\Core\Url
   *   Populated url object.
   */
  public function fromUserInput($url, array $options = []): Url;

  /**
   * Wrapper for Url from route.
   *
   * @param string $routeName
   *   Route name.
   * @param array $routeParameters
   *   Route parameters (optional).
   * @param array $options
   *   Options array (optional).
   *
   * @return \Drupal\Core\Url
   *   Populated url object.
   */
  public function fromRoute($routeName, array $routeParameters = [], array $options = []): Url;

  /**
   * Wrapper for Url from uri.
   *
   * @param string $uri
   *   The uri.
   * @param array $options
   *   Options array (optional).
   *
   * @return \Drupal\Core\Url
   *   Populated url object.
   */
  public function fromUri(string $uri, array $options = []): Url;

}
