<?php

namespace Drupal\vm_services\Factory;

/**
 * Interface DateTimeFactoryInterface.
 *
 * @package Drupal\vm_services\Factory
 */
interface DateTimeFactoryInterface {

  /**
   * Create immutable date time.
   *
   * @param string $time
   *   Time.
   * @param \DateTimeZone|null $timezone
   *   Timezone.
   *
   * @return \DateTimeImmutable
   *   Immutable date time.
   */
  public function createImmutable($time = 'now', \DateTimeZone $timezone = NULL): \DateTimeImmutable;

}
