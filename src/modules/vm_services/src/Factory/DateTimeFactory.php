<?php

namespace Drupal\vm_services\Factory;

/**
 * Class DateTimeFactory.
 *
 * @package Drupal\vm_services\Factory
 */
final class DateTimeFactory implements DateTimeFactoryInterface {

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function createImmutable($time = 'now', \DateTimeZone $timezone = NULL): \DateTimeImmutable {
    return new \DateTimeImmutable($time, $timezone);
  }

}
