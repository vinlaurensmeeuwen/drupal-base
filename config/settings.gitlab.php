<?php

$databases['default']['default'] = [
  'database' => 'testing',
  'username' => 'runner',
  'password' => 'runner',
  'prefix' => '',
  'host' => 'mysql',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
];

$settings['config_sync_directory'] = '../config/sync';
